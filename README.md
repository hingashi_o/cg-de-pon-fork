# cg-de-pon

## 説明

AI画像生成プロンプトまとめwiki [わがままAI CGでポン！](https://seesaawiki.jp/cg-de-pon/)の拡張jsなどを管理するリポジトリです。
コードは [novelai 5ch wiki拡張jsリポジトリ](https://github.com/naisd5ch/novel-ai-5ch-wiki-js) をコピー・改修しています。

## 配信方法

mainブランチがgitlab pagesで公開され、 wikiのフリーエリアのこのhtmlによりインポートされます。

```
<!--
    バグ報告、提案、PRはここまでお願いします。
    https://gitlab.com/senka-ai/cg-de-pon/
-->
<link href="https://senka-ai.gitlab.io/cg-de-pon/prompt-table.css" rel="stylesheet">
<script src="https://senka-ai.gitlab.io/cg-de-pon/prompt-table.js"></script>
```

## 自分も修正できるぜ！したいぜ！という人へ

GitLabのアカウントがあれば特に権限とかはなくできるはず。
修正の場合は履歴管理用に<s>プルリク</s>マーリク作ってもらえるといいかも。
専科の報告・要望をまとめたIssue作成だけでもありがたいです。
